import React from 'react';
import clsx from 'clsx';
import styles from './HomepageFeatures.module.css';

const FeatureList = [
  {
    title: 'Easy to use',
    Svg: require('../../static/img/settings_white_24dp.svg').default,
    description: (
      <>
        Drive Mode Dashboard is really easy to use.
      </>
    ),
  },
  {
    title: 'Focus on what matters',
    Svg: require('../../static/img/settings_white_24dp.svg').default,
    description: (
      <>
        Customize your dashboard and focus on the data you care about.
      </>
    ),
  },
  {
    title: 'Universally compatible',
    Svg: require('../../static/img/settings_white_24dp.svg').default,
    description: (
      <>
        Use Drive Mode Dashboard on any Android device.
      </>
    ),
  },
];

function Feature({Svg, title, description}) {
  return (
    <div className={clsx('col col--4')}>
      <div className="text--center">
        <Svg className={styles.featureSvg} alt={title} />
      </div>
      <div className="text--center padding-horiz--md">
        <h3>{title}</h3>
        <p>{description}</p>
      </div>
    </div>
  );
}

export default function HomepageFeatures() {
  return (
    <section className={styles.features}>
      <div className="container">
        <div className="row">
          {FeatureList.map((props, idx) => (
            <Feature key={idx} {...props} />
          ))}
        </div>
      </div>
    </section>
  );
}
