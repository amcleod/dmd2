import React from 'react';
import clsx from 'clsx';
import Layout from '@theme/Layout';
import Link from '@docusaurus/Link';
import useDocusaurusContext from '@docusaurus/useDocusaurusContext';
import styles from './index.module.css';
import HomepageFeatures from '../components/HomepageFeatures';
import Translate from '@docusaurus/Translate';

function HomepageHeader() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <header className={clsx('hero hero--primary', styles.heroBanner)}>
      <div className="container">
        <h1 className="hero__title">{siteConfig.title}</h1>
        <p className="hero__subtitle"><Translate>
          A complete dashboard for your motorcycle.
          </Translate></p>
        <div className={styles.buttons}>
          <Link
            className="button button--secondary button--lg"
            to="/docs/intro">
            <Translate>
              Get support now ⏱️
            </Translate>
          </Link>
        </div>
      </div>
    </header>
  );
}

export default function Home() {
  const {siteConfig} = useDocusaurusContext();
  return (
    <Layout
      title={`Support for ${siteConfig.title}`}
      description="Drive Mode Dashboard user guide>">
      <HomepageHeader />
      <main>
        <div className={clsx('col col--12"')}>
          <div className="home-feature-image">
            <img src='img/dmd-allviews.png' alt="Easy to use" width="80%"/>
          </div>
          <div className={clsx('col col--6"')} class="home-feature-text">
            <h3>
              <Translate description="Easy to use description text">
                Easy to use
              </Translate>
            </h3>
            <p>
              <Translate>
                Drive Mode Dashboard is really easy to use.
              </Translate>
            </p>
          </div>
          <div className={clsx('col col--6"')} class="home-feature-text">
            <h3>
              <Translate description="Focus description text">
                Focus on what matters
              </Translate>
            </h3>
            <p>
              <Translate>
                Customize your dashboard and focus on the data you care about.
              </Translate>
            </p>
          </div>
          <div className={clsx('col col--6"')} class="home-feature-text">
            <h3>
              <Translate description="Universally compatible description text">
                Universally compatible
              </Translate>
            </h3>
            <p>
              <Translate>
                Drive Mode Dashboard is compatible with all Android devices.
              </Translate>
            </p>
          </div>
        </div>
      </main>
    </Layout>
  );
}
