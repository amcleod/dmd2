---
sidebar_position: 1
---

# Configuring maps in Drive Mode Dashboard

You can customize various aspects of the Drive Mode Dashboard maps.

## Downloading maps

Download maps for your geography so that you can access the maps without an internet connection.

## Configuring map layers

Enable different map layers to display different types of information in the map.

## Configuring map widgets

You can include specific information overlaid on the map screen.

## Using maps with a CARPE handlebar controller

Drive Mode Dashboard maps supports navigation with a handlebar controller.

## Storing maps on an SD card

You can store maps on an SD card.

## Map legend

Map legend reference topic.
