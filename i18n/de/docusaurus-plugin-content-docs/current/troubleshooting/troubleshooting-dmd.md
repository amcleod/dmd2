---
sidebar_position: 1
---

# Troubleshooting Drive Mode Dashboard

If you encounter issues with Drive Mode Dashboard, turn your device off and on.

## Configuring compass heading

Use GPS heading instead of magnetic heading.

## Diagnosing power supply issues

Use the Ampere application to monitor charge status.

## Configuring screen sensitivity

Your tablet might register raindrops as touchscreen input. Configure the screen sensitivity to counteract this effect.
