---
sidebar_position: 1
---

# Pairing a controller

Drive Mode Dashboard is compatible with CARPE handlebar controllers from Thork Racing.

## Pairing the Adventure Control controller

Bluetooth.

## Pairing the Terrain Command controller

Bluetooth.
