---
sidebar_position: 2
---

# Navigating a roadbook

Drive Mode Dashboard supports importing a PDF roadbook.

## Advancing the roadbook

- Swipe the screen.
- Use the handlebar controller.

## Setting trip distances

- Tap the screen to increment, decrement, or reset trip distances.
- Use the Adventure Control or Command Terrain controllers to increment, decrement, or reset trip distances.

## Map legend

Map legend reference topic.
