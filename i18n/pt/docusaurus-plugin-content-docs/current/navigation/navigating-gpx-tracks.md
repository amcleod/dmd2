---
sidebar_position: 2
---

# Navigating GPX routes

Drive Mode Dashboard supports importing and recording GPX routes.

## Recording GPX routes

Record your favourite routes.

## Importing GPX routes

Enable different map layers to display different types of information in the map.

## Following GPX tracks

Use your GPS location to follow a GPX routes that you have imported to the Drive Mode Dashboard map.

## Map legend

Map legend reference topic.
