---
sidebar_position: 3
---

# Navigating GPX tracks

Drive Mode Dashboard supports importing and recording GPX tracks.

## Recording GPX tracks

Record your favourite tracks.

## Importing GPX tracks

Enable different map layers to display different types of information in the map.

## Following GPX tracks

Use your GPS location to follow a GPX track that you have imported to the Drive Mode Dashboard map.

## Map legend

Map legend reference topic.
