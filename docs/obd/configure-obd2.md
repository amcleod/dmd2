---
sidebar_position: 1
---

# Configuring OBD2 connections

Integrate live data from your ECU into your dashboard.

## Installing an OBD2 reader on your motorcycle.

1. Connect the motorcycle-specific OBD2 adaptor to the port on your bike.
1. Connect the Bluetooth or WiFi OBD2 reader to the adaptor.

## Pairing a Bluetooth OBD2 reader with Drive Mode Dashboard

1. Pair your device with your Bluetooth OBD2 reader.
1. Navigate to the OBD screen in Drive Mode Dashboard.
1. Tap the menu panel on the left side of the screen and select **OBD View Setup**.
1. Tap **Start OBD Connection**.
1. Tap **Select ELM327 Bluetooth Device**.
1. Tap the OBD2 reader that you want to connect to Drive Mode Dashboard.


## Pairing a WiFi OBD2 reader with Drive Mode Dashboard


## Enabling and disabling OBD2 sensor data

## Configuring speed and RPM

## Configuring fuel range

## Configuring the gear indicator

## OBD2 protocol compatibility

Drive Mode Dashboard supports the following OBD2 protocols:
- SAE_J1850_PWM
- SAE_J1850_VPW
- ISO_9141_2
- ISO_14230_4_KWP
- ISO_14230_4_KWP_FAST
- ISO_15765_4_CAN
- ISO_15765_4_CAN_B
- ISO_15765_4_CAN_C
- ISO_15765_4_CAN_D
- SAE_J1939_CAN
- USER1_CAN
- USER2_CAN
