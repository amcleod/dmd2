---
sidebar_position: 2
---

# Configuring the dashboard layout

The Drive Mode Dashboard layout is highly customizable.

## Customizing the dock

Add or remove shortcuts from the dock at the bottom of the screen.

## Customizing the widgets

Configure your dashboard to display the information that you want to see.

## Configuring app shortcuts

Configure your dashboard to include shortcuts to specific applications.

## Configuring units of measurement

Configure metric or imperial units of measurement.

## Locking the screen orientation

Lock the screen in landscape or portrait mode.
